## Inhalt/Kurzbeschreibung
 
+ **`klingel.py`**\
    Haupt-Interface zur Klingel. Details im nächtsten Abschnitt
   
+ **`update_bk.py`**\
    Liest Text aus `bk.json` und versucht, diesen per BLE auf das Briefkastenschild zu schreiben.
    
---
+ **`bk_schild.py`**\
    Interface-Klasse `Schild` zum Briefkastenschild. Hängt ab von `bluepy` und funktioniert nur auf dem RPi.
    
+ **`epd.py`**\
    Drei Interface-Klassen um die angeschlossenen Epaper-Displays zu verwalten und beschreiben:
        
    * `OneEPD`\
        verwaltet ein einzelnes Display
    * `DoubleEPD`\
        verwaltet zwei Displays zusammen
    * `EpaperHandler`\
        Generiert alle Pixeldaten und teilt sie auf die Displays auf.
        Diese Klasse ist das Haupt-Interface zu den Displays.
    
+ **`fontrender.py`**\
    Beinhaltet wrapper um `freetype`. Wird von `EpaperHandler` verwendet.
---    
+ **`flasktest.py`**\
    Schnittstelle zwischen web-frontend und python.
---
+ **`names.json`**\
    Texte für die Klingel-Displays. Details zum Format im Abschnitt weiter unten.
    
+ **`bk.json`**\
    Texte für das Briefkastenschild. Details zum Format im Abschnitt weiter unten.
---
 + **`200210_ehren_protokoll.ods`**\
    Beschreibt das Protokoll zur Kommunikation mit dem Briefkastenschild über BLE    

## klingel.py
Dieses Script ist ein Wrapper um einen `EpaperHandler` und stellt einige Funktionen über CLI-Argumente zur Verfügung.

`update`\
Liest die Namen aus `names.json` ein und stellt diese auf den Displays dar.
    
`init`\
    Erstellt eine default `names.json` und stellt den Inhalt auf den Displays dar.
    
`list`\
    Liest die Namen aus `names.json` und gibt diese aus, ohne auf das Display zu schreiben.
    
**`setname`**` pos name`\
    Subcommand um einen Namen per CLI zu setzen. Hat folgende Argumente:

 `pos [1-8]`\
    Stelle, an die der neue Name geschrieben werden soll. 
    
    
 `name`\
    Der String, der an Stelle `pos` geschrieben werden soll


## names.json
In dieser Datei werden die Inhalte für die Darstellung auf den Displays gespeichert. Sie folgt diesem Format:


```json
[
    "Partei A",
    "Partei B",
    "Partei C",
    "Partei D",
    "Partei E",
    "Partei F",
    "Partei G",
    "Partei H"
]
```

Jeder Eintag kann mehrere Zeilen beinhalten:

```json
[
  "Partei A1\nPartei A2",
  "Partei B",
  ...
]
```



## bk.json
In dieser Datei werden die Inhalte für die Darstellung das Briefkastenschild gespeichert. Sie folgt diesem Format:

```json
[
  "Zeile 1",
  "Zeile 2",
  "Zeile 3",
  "Zeile 4",
  "Zeile 5"
]
```

Hier dürfen Zeilen keine Umbrüche enthalten.



## Raspberry Pi Setup
Folgende Schritte sind notwendig um das System neu aufzusetzen:

1. Raspbian installieren
2. `sudo raspi-config` starten
    - expand filesystem
    - enable SPI
    - (Kennwort ändern)
3. Abhängigkeiten installieren:
    `pip3 install bluepy Flask freetype-py Pillow RPi.GPIO spidev waitress Werkzeug Jinja2`
4. dieses repository in `/home/pi/ehren` bereitstellen:
    ```
   cd /home/pi/
   git clone https://gitlab.com/fstein/ehren.git
    ```
5. Rezept in [readme_serverConfig.md](readme_serverConfig.md) befolgen um den Autostart, Webserver und DNS-Server zu konfigurieren

## Hardwarebeschreibung
In der Klingel sind diese Module verbaut:
+ Raspberry Pi Zero W
+ 2 x Waveshare E-Paper HAT
+ 2 x 4.2" Waveshare E-Paper Displays
+ Adafruit Feather nrf52
+ 2.9" Waveshare E-Paper Modul

Um die beiden E-Paper HATs gleichzeitig am Raspberry Pi betreiben zu können, sind alle Leitungen bis auf zwei parallel miteinander verbunden:
```mermaid
graph TB
B---E & H
A---D
I---F
J---C
K---G
subgraph Raspberry Pi Zero W
A(Pin 5)
I(Pin 17)
J(Pin 7)
K(Pin 8)
B(SPI)
end
subgraph E-Paper Hat 1
E(SPI)
C(CS)
D(RST)

end
subgraph E-Paper Hat 2
F(RST)
G(CS)
H(SPI)
end
```
Die Verdrahtung ist in diesem Bild gut zu sehen:
![](img/200318_usb.jpg)


Dieses Diagramm beschreibt des gesamten Systemaufbau:
```mermaid
graph TB

subgraph Klingel
F(DC 5V)
A(Raspberry Pi Zero W)
B(E-Paper module 1)
C(E-Paper module 2)
D(Display 1)
E(Display 2)
F-->|USB|A

A-->|SPI, CS Pin 7, RST Pin 5|B
A-->|SPI, CS Pin 8, RST Pin 17|C

B-->|FPC|D
C-->|FPC|E
end
subgraph Briefkastenschild
G(Adafruit Feather nrf52)
H(Waveshare 2.9 E-paper Module)
I(DC 3V)

I-->G
A-->|BLE|G
G-->H
end

subgraph Endgerät
J(Smartphone oder Laptop)
J-->|WIFI|A
end

subgraph Gira-Modul
M(Gira Systembus)-->K
K(Gira Klingelmodul)-->L(8 Taster)
end
```