
# IPTABLES Konfiguration und Persistierung

```sh
	sudo iptables -t nat -A PREROUTING -i wlan0 -p tcp --dport 80 -j REDIRECT --to-ports 5000
	sudo apt-get install iptables-persistent
```

# WAITRESS WEBSERVER

```sh
	pip3 install WAITRESS
	waitress-serve --port=5000 flasktest:app # wobei dateiname:flask_variable_der_flask_app
```

# SYSTEMD (/etc/)

```sh
	[Unit]
	Description=Klingel
	After=network.target

	[Service]
	User=pi
	Type=simple
	WorkingDirectory=/home/pi/ehren
	ExecStart=sudo -E /home/pi/.local/bin/waitress-serve --port=5000 flasktest:app
	#Restart=never

	[Install]
	WantedBy=multi-user.target
```

# DNSMASQ and HOSTAPD
## /etc/dhcpcd.conf
```sh
# A sample configuration for dhcpcd.
# See dhcpcd.conf(5) for details.

# Allow users of this group to interact with dhcpcd via the control socket.
#controlgroup wheel

# Inform the DHCP server of our hostname for DDNS.
hostname

# Use the hardware address of the interface for the Client ID.
clientid
# or
# Use the same DUID + IAID as set in DHCPv6 for DHCPv4 ClientID as per RFC4361.
# Some non-RFC compliant DHCP servers do not reply with this set.
# In this case, comment out duid and enable clientid above.
#duid

# Persist interface configuration when dhcpcd exits.
persistent

# Rapid commit support.
# Safe to enable by default because it requires the equivalent option set
# on the server to actually work.
option rapid_commit

# A list of options to request from the DHCP server.
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
# Respect the network MTU. This is applied to DHCP routes.
option interface_mtu

# Most distributions have NTP support.
#option ntp_servers

# A ServerID is required by RFC2131.
require dhcp_server_identifier

# Generate SLAAC address using the Hardware Address of the interface
#slaac hwaddr
# OR generate Stable Private IPv6 Addresses based from the DUID
slaac private

# Example static IP configuration:
#interface eth0
#static ip_address=192.168.0.10/24
#static ip6_address=fd51:42f8:caae:d92e::ff/64
#static routers=192.168.0.1
#static domain_name_servers=192.168.0.1 8.8.8.8 fd51:42f8:caae:d92e::1

# It is possible to fall back to a static IP if DHCP fails:
# define static profile
#profile static_eth0
#static ip_address=192.168.1.23/24
#static routers=192.168.1.1
#static domain_name_servers=192.168.1.1

# fallback to static profile on eth0
#interface eth0
#fallback static_eth0

denyinterfaces wlan0
```

# /etc/network/interfaces

```sh
allow-hotplug wlan0  
iface wlan0 inet static  
    address 192.168.1.1
    netmask 255.255.255.0
    network 192.168.1.0
    broadcast 192.168.1.255
```

# HOSTAPD /etc/hostapd/hostapd.conf
```sh

# This is the name of the WiFi interface we configured above
interface=wlan0

# Use the nl80211 driver with the brcmfmac driver
driver=nl80211

# This is the name of the network
ssid=klingel

# Use the 2.4GHz band
hw_mode=g

# Use channel 6
channel=6

# Enable 802.11n
ieee80211n=1

# Enable WMM
wmm_enabled=1

# Enable 40MHz channels with 20ns guard interval
ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]

# Accept all MAC addresses
macaddr_acl=0

# Use WPA authentication
auth_algs=1

# Require clients to know the network name
ignore_broadcast_ssid=0

# Use WPA2
wpa=2

# Use a pre-shared key
wpa_key_mgmt=WPA-PSK

# The network passphrase
wpa_passphrase=ehrenklingel

# Use AES, instead of TKIP
rsn_pairwise=CCMP

```

# DNSMASQ

```sh
	interface=wlan0
	listen-address=192.168.1.1
	bind-interfaces
	domain-needed
	bogus-priv
	dhcp-range=192.168.1.2,192.168.1.150,12h
	address=/#/192.168.1.1
	no-resolv

```

# Deaktivierung

 - Absatz in `/etc/network/interfaces` auskommentieren
 - `sudo systemctl disable hostapd`
 - `sudo systemctl disable dnsmasq`
 - `sudo service dnsmasq stop`
 - `denyinterfaces wlan0` in `/etc/dhcpcd.conf` auskommentieren
 - `sudo reboot`

# Aktivierung

 - Absatz in `/etc/network/interfaces` einkommentieren
 - `sudo systemctl enable hostapd`
 - `sudo systemctl enable dnsmasq`
 - `sudo service dnsmasq start`
 - `denyinterfaces wlan0` in `/etc/dhcpcd.conf` einkommentieren
 - `sudo reboot`
