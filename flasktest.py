#!/usr/bin/python3

from flask import Flask, render_template, jsonify, request
import json
import time

import epd
from bk_schild import Schild

app = Flask(__name__)

@app.route("/")
def interactive():
    try:
        with open("names.json", "r", encoding="UTF-8") as f:
            _names = json.load(f)
        with open("bk.json", "r", encoding="UTF-8") as bkf:
            _postboxlines = json.load(bkf)
        return render_template("interactive.html", names=_names, postboxlines=_postboxlines)
    except Exception as e:
        return str(e)

@app.route("/_background_process", methods=['POST'])
def background_process():
    try:
        with open("names.json", "w", encoding="UTF-8") as f:
          json.dump(request.get_json(), f, ensure_ascii=False, indent=4)
        f.close()
        #time.sleep(5)
        display = epd.EpaperHandler()
        _names = display.names
        assert _names is not None, "no names to be displayed"
        display.update(_names)
        return jsonify(result="ok")

    except Exception as e:
        return str(e)

@app.route("/_postbox", methods=['POST'])
def postbox():
    try:
        with open("bk.json", "w", encoding="UTF-8") as f:
          json.dump(request.get_json(), f, ensure_ascii=False, indent=4)
        f.close()
        #time.sleep(5)
        ks = Schild()
        t = 30
        print("trying to connect with timeout: {}s".format(t))
        ks.update(t)
        return jsonify(result="ok")

    except Exception as e:
        return str(e)
    

if "__name__" =="__main__":
    app.run(host="0.0.0.0")
