## Zusammenfassung
Dieses Repository ist Teil der Hausklingel und beinhaltet alle relevanten Informationen um die Klingel in Betrieb zu nehmen, zu bedienen und eine Dokumentation aller technischen Bestandteile.
## Inbetriebnahme
#### Klingel
1. Sicherungsschraube am unteren Ende herausschrauben
1. Montageblech an die Wand anschrauben. Dazu müssen nicht alle Montagelöcher verwendet werden.\
Das Blech sollte nicht in der Wand einliegen und sich bei der Montage nicht verziehen.
2. Gira-Modul an die Klingelanlage anschließen
3. Raspberry Pi an ein USB-Netzteil (5V DC 500mA) anschließen. Dazu den USB micro-B Port verwenden, der mit dem roten Pfeil gekennzeichnet ist:


![](img/200318_usb.jpg)
4. Klingelgehäuse an der Oberkante einhängen
5. Klingelgehäuse anklappen
6. Sicherungsschraube am unteren Ende  befestigen

#### Briefkastenschild

Das Briefkastenschild besteht aus zwei Teilen: Dem Schild selbst und dem BLE-Modul. Die beiden Teile sind mit einem Flachbandkabel und 10-Pol Stecker verbunden. Dieser ist verpolungssicher und kann bedenkenlos getrennt werden.\
Zur Inebtriebnahme des Klingelschilds
1. Zwei AAA-Batterien mit je 1.5V in den Batteriehalter einlegen
2. Schild und BLE-Modul am 10-Pol Stecker trennen
3. Das Schild aussen montieren und das Kabel nach innen führen
4. Das BLE-Modul innen montieren und bei der Montage den Stecker wieder anbringen

 
## Nutzung
Zur Änderung der Klingelbeschriftung geht man wie folgt vor:
1. In unmittelbarer Nähe zur Klingel im WLAN anmelden:
    ```
   SSID: klingel
   passphrase: ehrenklingel
   ```
    Das Wlan stellt kein Internet zur Verfügung, was bei den meisten Endgeräten zu einem Warnhinweis führt. Unter Umständen muss man explizit bestätigen, mit dem Netzwerk verbunden zu bleiben.

2. In einem Browser eine beliebige web-Adresse öffnen. Alle Anfragen werden auf das Kontrollinterface umgeleitet.
3. Im Kontrollinterface können pro Klingeltaster bis zu drei Zeilen eingegeben werden. Die Zahl der Zeilen lässt sich mit `+` und `-` ändern.
4. Nach der Eingabe aller Änderungen schreibt man mit dem Button `Klingelschilder Speichern` auf die Displays.
Dieser Vorgang dauert eine Weile.

Zur Änderung der Briefkastenbeschriftung geht man wie folgt vor:
1. Punkte 1 und 2 wie zuvor.
2. Im Kontrollinterface können die Inhalte von fünf Zeilen eingegeben werden.
3. Das Drücken auf `Briefkasten Speichern` startet die Suche nach dem Briefkastenschild. Innerhalb dieser Suche muss man am Briefkasten den Knopf mit der Beschriftung `reset` drücken. Die Suche nach dem Briefkastenschild dauert maximal 30s.
4. Ist der Vorgang nicht erfolgreich, sind unbedingt die Batterien zu überprüfen.

## Technische Referenz
Alle technisch relevanten Inhalte sind in der Datei [readme_technical.md](readme_technical.md) zu finden.
