import json
import logging
from fontrender import Font
from PIL import Image
import time
import RPi.GPIO


class OneEPD:
    """
    Basisklasse für ein einzelnes Display
    """

    RST_PIN = None  # soll beim Setup gesetzt werden
    DC_PIN = 25
    CS_PIN = None  # soll beim Setup gesetzt werden
    BUSY_PIN = 24

    WIDTH = 400
    HEIGHT = 300

    def __init__(self, RST, CS):
        self.RST_PIN = RST
        self.CS_PIN = CS

        import spidev
        import RPi.GPIO

        self.GPIO = RPi.GPIO

        # SPI device, bus = 0, device = 0
        self.SPI = spidev.SpiDev(0, 0)

    def reset(self):
        self.digital_write(self.RST_PIN, 1)
        self.delay_ms(200)
        self.digital_write(self.RST_PIN, 0)
        self.delay_ms(10)
        self.digital_write(self.RST_PIN, 1)
        self.digital_write(self.CS_PIN, 1)
        self.delay_ms(200)

    def send_command(self, command):
        self.digital_write(self.DC_PIN, 0)
        self.digital_write(self.CS_PIN, 0)
        self.spi_writebyte([command])
        self.digital_write(self.CS_PIN, 1)

    def send_data(self, data):
        self.digital_write(self.DC_PIN, 1)
        self.digital_write(self.CS_PIN, 0)
        self.spi_writebyte([data])
        self.digital_write(self.CS_PIN, 1)

    def read_busy(self):
        self.send_command(0x71)
        while (self.digital_read(self.BUSY_PIN) == 0):  # 0: idle, 1: busy
            self.send_command(0x71)
            self.delay_ms(100)

    def digital_write(self, pin, value):
        self.GPIO.output(pin, value)

    def digital_read(self, pin):
        return self.GPIO.input(pin)

    def delay_ms(self, delaytime):
        time.sleep(delaytime / 1000.0)

    def spi_writebyte(self, data):
        self.SPI.writebytes(data)

    def get_buffer(self, image):
        # logging.debug("bufsiz = ",int(self.width/8) * self.height)
        buf = [0xFF] * (int(self.WIDTH / 8) * self.HEIGHT)
        image_monocolor = image.convert('1')
        imwidth, imheight = image_monocolor.size
        pixels = image_monocolor.load()
        # logging.debug("imwidth = %d, imheight = %d",imwidth,imheight)
        if (imwidth == self.WIDTH and imheight == self.HEIGHT):
            for y in range(imheight):
                for x in range(imwidth):
                    # Set the bits for the column of pixels at the current position.
                    if pixels[x, y] == 0:
                        buf[int((x + y * self.WIDTH) / 8)] &= ~(0x80 >> (x % 8))
        elif (imwidth == self.HEIGHT and imheight == self.WIDTH):
            for y in range(imheight):
                for x in range(imwidth):
                    newx = y
                    newy = self.HEIGHT - x - 1
                    if pixels[x, y] == 0:
                        buf[int((newx + newy * self.WIDTH) / 8)] &= ~(0x80 >> (y % 8))
        return buf

    def display(self, image):
        self.send_command(0x10)
        for i in range(0, int(self.WIDTH * self.HEIGHT / 8)):
            self.send_data(0xFF)

        self.send_command(0x13)
        for i in range(0, int(self.WIDTH * self.HEIGHT / 8)):
            self.send_data(image[i])

        self.send_command(0x12)
        self.read_busy()

    def send_image(self, image):
        _buf = self.get_buffer(image)
        self.send_command(0x10)
        for i in range(0, int(self.WIDTH * self.HEIGHT / 8)):
            self.send_data(0xFF)

        self.send_command(0x13)
        for i in range(0, int(self.WIDTH * self.HEIGHT / 8)):
            self.send_data(_buf[i])

        self.read_busy()

    def display_image(self):
        self.send_command(0x12)
        self.read_busy()

    def clear(self):
        self.send_command(0x10)
        for i in range(0, int(self.WIDTH * self.HEIGHT / 8)):
            self.send_data(0xFF)

        self.send_command(0x13)
        for i in range(0, int(self.WIDTH * self.HEIGHT / 8)):
            self.send_data(0xFF)

        self.send_command(0x12)
        self.read_busy()

    def sleep(self):
        self.send_command(0x02)  # POWER_OFF
        self.read_busy()
        self.send_command(0x07)  # DEEP_SLEEP
        self.send_data(0XA5)

    def module_init(self):

        self.GPIO.setup(self.RST_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.DC_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.CS_PIN, self.GPIO.OUT)
        self.GPIO.setup(self.BUSY_PIN, self.GPIO.IN)
        return 0

    def module_exit(self):
        self.GPIO.output(self.RST_PIN, 0)
        self.GPIO.output(self.RST_PIN, 0)
        self.GPIO.output(self.DC_PIN, 0)


class DoubleEPD:
    """
    interface um zwei Displays am selben SPI-bus zu betreiben.
    Ein CS und ein RST pin pro display wird benötigt, die CS-Funktion vom SPI-bus muss deaktiviert werden (siehe Zeile 167)
    """

    def __init__(self):
        self.top = OneEPD(5, 7)
        self.bottom = OneEPD(17, 8)
        import spidev
        self.GPIO = RPi.GPIO
        # SPI device, bus = 0, device = 0
        self.SPI = spidev.SpiDev(0, 0)
        self.SPI.no_cs = True  # chip select wird über normale GPIO gesetzt

    lut_vcom0 = [
        0x00, 0x17, 0x00, 0x00, 0x00, 0x02,
        0x00, 0x17, 0x17, 0x00, 0x00, 0x02,
        0x00, 0x0A, 0x01, 0x00, 0x00, 0x01,
        0x00, 0x0E, 0x0E, 0x00, 0x00, 0x02,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]
    lut_ww = [
        0x40, 0x17, 0x00, 0x00, 0x00, 0x02,
        0x90, 0x17, 0x17, 0x00, 0x00, 0x02,
        0x40, 0x0A, 0x01, 0x00, 0x00, 0x01,
        0xA0, 0x0E, 0x0E, 0x00, 0x00, 0x02,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]
    lut_bw = [
        0x40, 0x17, 0x00, 0x00, 0x00, 0x02,
        0x90, 0x17, 0x17, 0x00, 0x00, 0x02,
        0x40, 0x0A, 0x01, 0x00, 0x00, 0x01,
        0xA0, 0x0E, 0x0E, 0x00, 0x00, 0x02,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]
    lut_wb = [
        0x80, 0x17, 0x00, 0x00, 0x00, 0x02,
        0x90, 0x17, 0x17, 0x00, 0x00, 0x02,
        0x80, 0x0A, 0x01, 0x00, 0x00, 0x01,
        0x50, 0x0E, 0x0E, 0x00, 0x00, 0x02,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]
    lut_bb = [
        0x80, 0x17, 0x00, 0x00, 0x00, 0x02,
        0x90, 0x17, 0x17, 0x00, 0x00, 0x02,
        0x80, 0x0A, 0x01, 0x00, 0x00, 0x01,
        0x50, 0x0E, 0x0E, 0x00, 0x00, 0x02,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]

    # ******************************gray*********************************/
    # 0~3 gray
    EPD_4IN2_4Gray_lut_vcom = [
        0x00, 0x0A, 0x00, 0x00, 0x00, 0x01,
        0x60, 0x14, 0x14, 0x00, 0x00, 0x01,
        0x00, 0x14, 0x00, 0x00, 0x00, 0x01,
        0x00, 0x13, 0x0A, 0x01, 0x00, 0x01,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    ]
    # R21
    EPD_4IN2_4Gray_lut_ww = [
        0x40, 0x0A, 0x00, 0x00, 0x00, 0x01,
        0x90, 0x14, 0x14, 0x00, 0x00, 0x01,
        0x10, 0x14, 0x0A, 0x00, 0x00, 0x01,
        0xA0, 0x13, 0x01, 0x00, 0x00, 0x01,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]
    # R22H	r
    EPD_4IN2_4Gray_lut_bw = [
        0x40, 0x0A, 0x00, 0x00, 0x00, 0x01,
        0x90, 0x14, 0x14, 0x00, 0x00, 0x01,
        0x00, 0x14, 0x0A, 0x00, 0x00, 0x01,
        0x99, 0x0C, 0x01, 0x03, 0x04, 0x01,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]
    # R23H	w
    EPD_4IN2_4Gray_lut_wb = [
        0x40, 0x0A, 0x00, 0x00, 0x00, 0x01,
        0x90, 0x14, 0x14, 0x00, 0x00, 0x01,
        0x00, 0x14, 0x0A, 0x00, 0x00, 0x01,
        0x99, 0x0B, 0x04, 0x04, 0x01, 0x01,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]
    # R24H	b
    EPD_4IN2_4Gray_lut_bb = [
        0x80, 0x0A, 0x00, 0x00, 0x00, 0x01,
        0x90, 0x14, 0x14, 0x00, 0x00, 0x01,
        0x20, 0x14, 0x0A, 0x00, 0x00, 0x01,
        0x50, 0x13, 0x01, 0x00, 0x00, 0x01,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    ]

    def init(self):
        self.SPI.max_speed_hz = 4000000
        self.SPI.mode = 0b00
        self.GPIO.setmode(self.GPIO.BCM)
        self.GPIO.setwarnings(False)
        self.top.module_init()
        self.bottom.module_init()
        self.top.reset()
        self.bottom.reset()

        for d in [self.top, self.bottom]:
            d.send_command(0x01)  # POWER SETTING
            d.send_data(0x03)  # VDS_EN, VDG_EN
            d.send_data(0x00)  # VCOM_HV, VGHL_LV[1], VGHL_LV[0]
            d.send_data(0x2b)  # VDH
            d.send_data(0x2b)  # VDL

        for d in [self.top, self.bottom]:
            d.send_command(0x06)  # boost soft start
            d.send_data(0x17)
            d.send_data(0x17)
            d.send_data(0x17)

        for d in [self.top, self.bottom]:
            d.send_command(0x04)  # POWER_ON
            d.read_busy()

        for d in [self.top, self.bottom]:
            d.send_command(0x00)  # panel setting
            d.send_data(0xbf)  # KW-BF   KWR-AF  BWROTP 0f
            d.send_data(0x0d)

        for d in [self.top, self.bottom]:
            d.send_command(0x30)  # PLL setting
            d.send_data(0x3c)  # 3A 100HZ   29 150Hz 39 200HZ  31 171HZ

        for d in [self.top, self.bottom]:
            d.send_command(0x61)  # resolution setting
            d.send_data(0x01)
            d.send_data(0x90)  # 128
            d.send_data(0x01)
            d.send_data(0x2c)

        for d in [self.top, self.bottom]:
            d.send_command(0x82)  # vcom_DC setting
            d.send_data(0x28)

        for d in [self.top, self.bottom]:
            d.send_command(0X50)  # VCOM AND DATA INTERVAL SETTING
            d.send_data(
                0x97)  # 97white border 77black border		VBDF 17|D7 VBDW 97 VBDB 57		VBDF F7 VBDW 77 VBDB 37  VBDR B7

        self.set_lut()
        # EPD hardware init end
        return 0

    def set_lut(self):
        for d in [self.top, self.bottom]:
            d.send_command(0x20)  # vcom
            for count in range(0, 44):
                d.send_data(self.lut_vcom0[count])

        for d in [self.top, self.bottom]:
            d.send_command(0x21)  # ww --
            for count in range(0, 42):
                d.send_data(self.lut_ww[count])

        for d in [self.top, self.bottom]:
            d.send_command(0x22)  # bw r
            for count in range(0, 42):
                d.send_data(self.lut_bw[count])

        for d in [self.top, self.bottom]:
            d.send_command(0x23)  # wb w
            for count in range(0, 42):
                d.send_data(self.lut_bb[count])
        for d in [self.top, self.bottom]:
            d.send_command(0x24)  # bb b
            for count in range(0, 42):
                d.send_data(self.lut_wb[count])

    def display(self, image):
        top_half = image.crop((0, 0, 300, 400))
        bottom_half = image.crop((0, 400, 300, 800))

        self.top.send_image(top_half)
        self.bottom.send_image(bottom_half)

        self.top.display_image()
        self.bottom.display_image()

    def sleep(self):
        self.top.sleep()
        self.bottom.sleep()

    def exit(self):
        self.SPI.close()
        self.top.module_exit()
        self.bottom.module_exit()
        self.GPIO.cleanup()


class EpaperHandler:
    """
    Generiert Pixeldaten zur Darstellung und schreibt die Daten auf die Displays.
    Diese Klasse sollte als einziges Interface zu den Displays dienen.
    """

    def __init__(self):
        self._names = None
        self.doubleDisplay = DoubleEPD()

    def init(self):
        self._names = ["1", "2", "3", "4", "5", "6", "7", "8"]
        self._save_names()

    def _read_names(self):
        try:
            with open("names.json", encoding="UTF-8") as f:
                buf = json.load(f)
                if not len(buf) == 8:
                    raise ValueError("list not 8 items long: [{}]".format(len(buf)))
                self._names = buf
                return 1
        except FileNotFoundError:
            logging.error("names.json not found. Use the -i option to create one")
            return -1

    def _save_names(self):
        with open("names.json", "w", encoding="UTF-8") as f:
            json.dump(self._names, f, ensure_ascii=False, indent=4)

    def update(self, names):
        try:
            logging.info("writing names to display:")
            logging.info(names)
            self.doubleDisplay.init()

            # some different font variations
            # fnt = Font("res/font/OpenSans-SemiBold.ttf", 30 * oversampling)
            # fnt = Font("res/font/D-DINExp-Bold.ttf", 32 * oversampling)
            # fnt = Font("res/font/Retron2000.ttf", 27 * oversampling)
            # fnt = Font("res/font/FreeMonoBold.ttf", 30 * oversampling)

            fnt = Font("res/font/FreeSans.ttf", 26)
            width = 300
            height = 800
            image = Image.new('1', (width, height), 0)  # 255: clear the frame

            leftPadding = 18
            vertPosA = 52
            vertPosB = 400+vertPosA
            vertSpacing = 99
            #yStep = (height - 2 * vertPadding) / 7
            namePos = [[leftPadding, vertPosA],
                       [leftPadding, vertPosA+1*vertSpacing],
                       [leftPadding, vertPosA+2*vertSpacing],
                       [leftPadding, vertPosA+3*vertSpacing],
                       [leftPadding, vertPosB],
                       [leftPadding, vertPosB+1*vertSpacing],
                       [leftPadding, vertPosB+2*vertSpacing],
                       [leftPadding, vertPosB+3*vertSpacing]
                       ]
            names = names

            for i in range(8):

                name = names[i]
                txt = fnt.render_text(name)

                image.paste(Image.frombytes("L", txt[1], txt[0].epapix()),
                             (namePos[i][0], round(namePos[i][1] - txt[1][1] / 2)))


                # image.paste(Image.frombytes("L", (50, 1), fnt.render_line(50)), (0, namePos[i][1]))

            image = image.rotate(180)
            self.doubleDisplay.display(image)

            time.sleep(2)

            self.doubleDisplay.sleep()
            logging.info("done")

        except IOError as e:
            logging.info(e)

    def exit(self):
        self.doubleDisplay.exit()

    @property
    def names(self):
        if self._read_names() > 0:
            return self._names

    @names.setter
    def names(self, names):
        buf = names
        assert len(buf) == 8, "length of list must be 8"
        for name in buf:
            assert type(name) is str, "names must be of type string"
            if len(name) <= 40:
                self._names = buf
                self._save_names()


if __name__ == "__main__":
    pass
