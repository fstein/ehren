import argparse
import logging
import epd

logging.basicConfig(level=logging.DEBUG)


def check_pos(value):  # helper func for input sanitizing
    p = int(value) - 1
    if p < 0 or p > 7:
        raise argparse.ArgumentTypeError("pos {} invalid. Range is 1-8".format(p + 1))
    return p


def main():  # parse arguments and map to class functions
    parser = argparse.ArgumentParser(description="updates nameplate epaper")
    parser.add_argument("-u", "--update", dest="update", action="store_true",

                        help="reads from \"names.json\" and updates display")
    parser.add_argument("-i", "--init", dest="init",
                        action="store_true", help="sets default names and updates display")
    parser.add_argument("-l", "--list", dest="list",
                        action="store_true", help="lists current names")
    subs = parser.add_subparsers(description="change one name", dest="setname", help="call klingel.py setname")
    setname = subs.add_parser("setname", description="set a new value for one name and update the display")
    setname.add_argument("pos", type=check_pos, help="[1-8] position to change")
    setname.add_argument("name", help="String to set as new name")
    args = parser.parse_args()
    display = epd.EpaperHandler()

    if args.init:
        display.init()
        display.update(display.names)

    elif args.list:
        print(display.names)

    elif args.update:
        _names = display.names
        assert _names is not None, "no names to be displayed"
        display.update(_names)

    elif args.setname:
        if display.names is None:
            display.init()

        _names = display.names
        _names[args.pos - 1] = args.newname
        display.names = _names
        print(display.names)
        print("set position {} to \"{}\"".format(args.pos, args.newname))

    else:
        parser.print_help()


if __name__ == "__main__":
    main()
