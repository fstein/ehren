from bluepy import btle
import struct
import time
import json

class KsScanDelegate(btle.DefaultDelegate):
    def __init__(self):
        self.foundKs = False
        btle.DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev and dev.addr == "ca:13:b5:88:c7:94":
            self.foundKs = True



class Schild:
    """
    Schnittstelle zum BLE Briefkastenschild
    """
    def __init__(self):
        self.mac_adress = "ca:13:b5:88:c7:94"
        self.p = btle.Peripheral()
        self.write_service = btle.Service
        self.write_char = btle.Characteristic
        self._connected = False
        self._lines = None

    def init(self):
        """
        erstellt eine default json-datei
        """
        self.lines = ["eins", "zwei", "drei", "vier", "fünf"]
        self._save_lines()

    def _save_lines(self):
        """
        speichert den aktuellen Zeileninhalt
        """
        with open("bk.json", "w", encoding="UTF-8") as f:
            json.dump(self._lines, f, ensure_ascii=False, indent=4)

    def _read_from_file(self):

        """
        versucht aus bk.json zu lesen
        """
        try:
            with open("bk.json", encoding="UTF-8") as f:
                buf = json.load(f)
                if not len(buf) == 5:
                    raise ValueError("list not 5 items long: [{}]".format(len(buf)))
                self._lines = buf
                return True
        except FileNotFoundError:
            return False

    def _connect(self, _timeout):
        """
        versucht, eine Verbindung mit dem Briefkastenschild aufzubauen. Funktion blockt für die über den timeout angegebene Dauer
        """
        scanner = btle.Scanner().withDelegate(KsScanDelegate())
        scanner.clear()
        scanner.start()
        ks_available = False
        count = 0
        while scanner.delegate.foundKs == False and count < _timeout:
            scanner.process(1)
            ks_available = scanner.delegate.foundKs
            count += 1
        scanner.stop()

        if ks_available:
            self.p.connect(self.mac_adress, addrType=btle.ADDR_TYPE_RANDOM, iface=0)
            self.write_service = self.p.getServiceByUUID("8d036225-4e0b-abb0-a449-12e111457388")
            self.write_char = self.write_service.getCharacteristics()[0]
            self._connected = True
            return True
        else:
            print("Klingelschild nicht gefunden, timeout")
            return False




    def _send_command(self, command):
        """
        überträgt Daten mit command-header entsprechend des dokumentierten Protokolls in 200210_ehren_protokoll.ods
        """
        if self._connected:
            self.write_char.write(struct.pack("BBB", 0x01, command, 0x00))

    def _send_data(self, line, data, add=False):
        """
        überträgt Daten mit data-header entsprechend des dokumentierten Protokolls in 200210_ehren_protokoll.ods
        """
        if self._connected:
            header = bytes([0x00, line, add])
            assembled = header + data
            if len(assembled) <= 20:
                out = struct.pack("<{}B".format(len(assembled)), *assembled)
                self.write_char.write(out)

    def update(self, timeout=20):
        """
        liest den Inhalt aus bk.json. Ist bk.json noch nicht vorhanden, wird eine default-datei erstellt.
        Überträgt und aktualisiert anschließend die Texte ans Briefkastenschild und aktualisiert das Display
        """
        if (not self._read_from_file()):
            self.init()
        if (self._connect(timeout)):
            print("connected")
            self.send_line(0, self._lines[0])
            self.send_line(1, self._lines[1])
            self.send_line(2, self._lines[2])
            self.send_line(3, self._lines[3])
            self.send_line(4, self._lines[4])
            self.update_display()
            self.p.disconnect()

    def send_line(self, line, data):
        """
        überträgt eine einzelne Zeile ans Briefkastenschild
        """
        if line >= 0 and line <= 4:
            _payload = bytes(data, "UTF-8")
            _segment = _payload[:17]
            _payload = _payload[17:]
            self._send_data(line, _segment)

            while len(_payload) > 0:
                _segment = _payload[:17]
                _payload = _payload[17:]
                self._send_data(line, _segment, add=True)
        else:
            print("\"line\" has to be between 0 and 4")

    def update_display(self):
        """
        Aktualisiert das Display
        """
        self._send_command(0x02)

    def clear_display(self):
        """
        Löscht den Inhalt des displays
        """
        self._send_command(0x01)


if __name__ == "__main__":
    pass
